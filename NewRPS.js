let batuEl = document.getElementById("batu-el")
let kertasEl = document.getElementById("kertas-el")
let guntingEl = document.getElementById("gunting-el")
let batuCOM = document.querySelector("#batu-com")
let kertasCOM = document.querySelector("#kertas-com")
let guntingCOM = document.querySelector("#gunting-com")
let hasilEl = document.querySelector("#hasil-el")
let hasilEl1 = document.querySelector("#hasil-el1")
let result = ""
let arrCOM = ["BATU", "KERTAS", "GUNTING"]
let isAlive = true

let fungsi = {
   getRandomValue: function() {
      let x = Math.floor(Math.random() * 3)
      return arrCOM[x]
   },

   hasil: function() {
      hasilEl1.textContent = result 
      hasilEl1.style.opacity = "1"
      hasilEl.style.opacity = "0"
   },

   beware: function() {
      alert("YOU HAVE PLAYED THE GAME, PRESS RESTART AT BOTTOM")
      console.log("YOU HAVE PLAYED THE GAME, PRESS RESTART AT BOTTOM")
   },
   
   restartGame: function() {
      location.reload()
   }
}

class GAME {
   constructor(pilihanCOM, pilihanPLAYER) {
      this.com = pilihanCOM
      this.player = pilihanPLAYER
   }

   deskripsi () {
      console.log(this.player + " adalah pilihan PLAYER")
      console.log(this.com + " adalah pilihan COMPUTER")
   }

   static checkResultBatu() {
      let COM = new GAME(fungsi.getRandomValue(), batuEl.alt)
      COM.deskripsi()
      if (batuEl.alt == "Batu" && COM.com == "BATU") {
         result = "DRAW"
         hasilEl1.style.backgroundColor = "darkgreen"
         console.log(result)
         batuCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
      } else if (batuEl.alt == "Batu" && COM.com == "KERTAS") {
         result = "COM WIN"
         console.log(result)
         kertasCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
      } else {
         result = "PLAYER 1 WIN"
         console.log(result)
         guntingCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
         
      }  
      fungsi.hasil()
      batuEl.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
   }
   
   static checkResultKertas() {
      let COM = new GAME(fungsi.getRandomValue(), kertasEl.alt)
      COM.deskripsi()
      if (kertasEl.alt == "Kertas" && COM.com == "BATU") {
         result = "PLAYER 1 WIN"
         console.log(result)
         batuCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
      } else if (kertasEl.alt == "Kertas" && COM.com == "KERTAS") {
         result = "DRAW"
         hasilEl1.style.backgroundColor = "darkgreen"
         console.log(result)
         kertasCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
      } else {
         result = "COM WIN"
         console.log(result)
         guntingCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
      } 
      fungsi.hasil()
      kertasEl.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
    }

   static checkResultGunting() {
      let COM = new GAME(fungsi.getRandomValue(), guntingEl.alt)
      COM.deskripsi()
       if (guntingEl.alt == "Gunting" && COM.com == "BATU") {
         result = "COM WIN"
         console.log(result)
         batuCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
      } else if (guntingEl.alt == "Gunting" && COM.com == "KERTAS") {
         result = "PLAYER 1 WIN"
         console.log(result)
         kertasCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
      } else {
         result = "DRAW"
         hasilEl1.style.backgroundColor = "darkgreen"
         console.log(result)
         guntingCOM.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
      }  
      fungsi.hasil()
      guntingEl.style.backgroundColor = "rgba(240, 248, 255, 0.504)"
   }
}

batuEl.addEventListener('click', function() { 
   if (isAlive === true) {
      GAME.checkResultBatu()
      isAlive = false
   } else {
      fungsi.beware()
   }
})

kertasEl.addEventListener('click', function() {
   if (isAlive === true) {
      isAlive = false
      GAME.checkResultKertas()
   } else {
      fungsi.beware ()
   }
})

guntingEl.addEventListener('click', function(){
   if (isAlive === true) {
      GAME.checkResultGunting()
      isAlive = false
   } else {
      fungsi.beware ()
   }
})









// function startGameBatu () {
//    let x = pilihan1
//    console.log(x + " adalah pilihan pemain" )
//    return x
// }

// function startGameKertas () {
//    let x = pilihan2
//    console.log(x + " adalah pilihan pemain" )
//    return x
// }

// function startGameGunting () {
//    let x = pilihan3
//    console.log(x + " adalah pilihan pemain" )
//    return x
// }






// let arr = ["BATU", "KERTAS", "GUNTING"]
// let batuEl = "Batu"

// function getRandomValue() {
//    let x = Math.floor(Math.random()*3) 
//    return arr[x]
   
// }

// console.log(getRandomValue())


// class Player {
//    constructor(batuEl) {
//       this.p1 = batuEl
//    }

//    deskripsi () {
//       console.log("player memilih " + this.p1)
//    }
// }

// let player1 = new Player(getRandomValue())
// player1.deskripsi()

// class COM {
//    constructor(pilihan) {
//       super(c1)
      
//    }
//    deskripsi () {
//       console.log("COM memilih " + this.p1)
//    }
// }

// let com1 = new Com(getRandomValue())
// com1.deskripsi()